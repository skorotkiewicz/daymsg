<?php

if ( $lang = 'deutsch' ) {
/* Deutsch language */
	define('LANG1', 'Bitcoin Preis stieg: ↑ ');
	define('LANG2', 'Bitcoin Preis senk: ↓ ');
	define('LANG3', 'Die aktuelle Bitcoin Preis: ');
	define('LANG4', 'Eine NEUE Version von Linux: ');
	define('LANG5', 'Die aktuelle Version von Linux: ');

} elseif ( $lang = 'polish' ) {
/* Deutsch language */
	define('LANG1', 'Cena bitcoin wzrosła: ↑ ');
	define('LANG2', 'Cena bitcoin spadła: ↓ ');
	define('LANG3', 'Cena bitcoin nie zmieniła się: ');
	define('LANG4', 'NOWA wersja Linux: ');
	define('LANG5', 'Aktualna wersja Linux: ');

} else {
/* Default english language */
	define('LANG1', 'Bitcoin price went up: ↑ ');
	define('LANG2', 'Bitcoin price went down: ↓ ');
	define('LANG3', 'The current Bitcoin price: ');
	define('LANG4', 'NEW version of Linux: ');
	define('LANG5', 'The current version of Linux: ');

}

