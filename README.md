DayMsg
=========

Your daily notifications on your phone

  - Can send notifications to any phone with support Pushover and NotifyMyAndroid
  - The ability to edit the look of any messages
  - Starting from your server via crontab

Thanks daymsg every morning you can see something that will inspire you for the day; a nice comment from twitter or cool joke


Version
----

0.3


Installation
--------------


    $ git clone http://git.itunix.eu/git/daymsg.git daymsg
    $ cd daymsg
    $ nano config-sample.php
    $ mv config-sample.php config.php


##### Configure crontab 
    $ crontab -e

##### Example cron
    0 8 * * * php /home/mod/skrypty/daymsg/example.php>/dev/null 2>&1

Example Output
--------------

    mod@aaa:~/skrypty/daymsg$ php example.php 

    Cześć Sebastian!

    Piątek, 25 kwietnia 2014 — Dzień Sekretarki, Międzynarodowy Dzień Świadomości Zagrożenia Hałasem
    ~~~
    Aktualna cena bitcoin: 95.89 €
    ~~~
    Aktualna wersja Linux: 3.14.1
    ~~~
    - The best part of UDP jokes is that I don't care if you get them.
    - I it. don't get
    ~~~
    Pogoda dla Fuldatal, HE, Fri, 25 Apr 2014 6:19 pm CEST
         - Obecna temperatura: 21*C
         - Prognoza
            Fri - Clear. High: 20*C Low: 8*C
            Sat - Scattered Thunderstorms. High: 19*C Low: 9*C
    ~~~


License
----

MIT

**Free Software, Hell Yeah!**
