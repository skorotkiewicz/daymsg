<?php

require_once('language.php');

//pushover
$token     = '';
$user      = ''; 
$device    = ''; //eg. iPad, iPhone, etc...

//notifymyandroid
$apikey    = '';

//twitter
define('CONSUMER_KEY', '');
define('CONSUMER_SECRET', '');

define('OAUTH_ACCESS_TOKEN', '');
define('OAUTH_TOKEN_SECRET', '');

define('OAUTH_CALLBACK', '');

//general settings
$event = '~~~ today\'s information ~~~';

// weather etc. 651095
$code = '651095';

// languages/ available: english, polish, deutsch
$lang = 'english';

?>
