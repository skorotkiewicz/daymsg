<?php
class _daymsg {
    // pushover notifications
    function Pushover($title, $message, $priority, $token, $user, $device) {
        $timestamp = time();
        $expire = 3600;
        $retry = 60;
        $sound = 'pushover';
        $callback = 'http://git.itunix.eu/git/daymsg.git';
        $url = 'http://git.itunix.eu/git/daymsg.git';
        $url_title = '~by your ITUnix.eu';
        $c = curl_init("https://api.pushover.net/1/messages.xml");
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($c, CURLOPT_HEADER, false);
        curl_setopt($c, CURLOPT_TIMEOUT, 6);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_POSTFIELDS, trim("token=$token&user=$user&title=$title&message=$message&device=$device&priority=$priority&timestamp=$timestamp&expire=$expire&retry=$retry&callback=$callback&url=$url&sound=$sound&url_title=$url_title"));
        curl_exec($c);
        curl_close($c);
    }

    // nma notifications
    function nma($apikey, $application, $event, $description, $url) {
        $c = curl_init("https://www.notifymyandroid.com/publicapi/notify");
        curl_setopt($c, CURLOPT_HEADER, 0);
        curl_setopt($c, CURLOPT_TIMEOUT, 6);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_POSTFIELDS, trim("apikey=$apikey&application=$application&event=$event&description=$description&url=$url"));
        curl_exec($c);
        curl_close($c);
    }

    // bulid the datebase if not exist
    function bulidDB() {
        $db = new PDO('sqlite:'.dirname(__FILE__).'/tmpka.db');
        if (filesize(dirname(__FILE__).'/tmpka.db') == 0) {
            $db->query("CREATE TABLE IF NOT EXISTS tmpka (
			            id INTEGER PRIMARY KEY, 
			            lasttweet TEXT,
			            lastkernel TEXT,
			            lastbitcoin TEXT
				    )");
            $db->query("INSERT INTO tmpka (lasttweet) VALUES ('tmp')");
            $db->query("INSERT INTO tmpka (lastkernel) VALUES ('tmp')");
            $db->query("INSERT INTO tmpka (lastbitcoin) VALUES ('tmp')");
        }
    }

    // get your last favorit tweet from user
    function zkalendarza() {
        require_once ('twitter/twitteroauth.php');
        $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, OAUTH_ACCESS_TOKEN, OAUTH_TOKEN_SECRET);
        $content = $connection->get('statuses/user_timeline', array('screen_name' => 'zkalendarza', 'count' => 1));
        $lasttweet = $content[0]->text;
        $db = new PDO('sqlite:'.dirname(__FILE__).'/tmpka.db');
        $sql = $db->query('SELECT lasttweet FROM tmpka')->fetch(PDO::FETCH_ASSOC);
        $sqltweet = $sql['lasttweet'];
        if ($lasttweet != $sqltweet) {
            $db->query("UPDATE tmpka SET lasttweet = '$lasttweet' WHERE id = 1");
            return $lasttweet;
        } else {
            return false;
        }
    }

    // get last bitcoin proce in euro
    function btceur() {
	$bitcoin = file_get_contents("https://api.bitcoinaverage.com/ticker/global/EUR/last");
        $db = new PDO('sqlite:'.dirname(__FILE__).'/tmpka.db');
        $sql = $db->query('SELECT lastbitcoin FROM tmpka')->fetch(PDO::FETCH_ASSOC);
        $sqlbitcoin = $sql['lastbitcoin'];
        if ($bitcoin > $sqlbitcoin) {
            $db->query("UPDATE tmpka SET lastbitcoin = '$bitcoin' WHERE id = 1");
            return LANG1 . $bitcoin . "€\n~~~";
        } elseif ($bitcoin < $sqlbitcoin) {
            $db->query("UPDATE tmpka SET lastbitcoin = '$bitcoin' WHERE id = 1");
            return LANG2 . $bitcoin . "€\n~~~";
        } else {
            return LANG3 . $bitcoin . "€\n~~~";
        }
    }

    // get last kernel info
    function kernel() {
        $c = curl_init("https://www.kernel.org/");
        curl_setopt($c, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0');
        curl_setopt($c, CURLOPT_HEADER, 1);
        curl_setopt($c, CURLOPT_TIMEOUT, 6);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $page = trim(curl_exec($c));
        curl_close($c);
        preg_match_all('#<a href="./pub/linux/kernel/(.*)/(.*)">(.*)</a>#msU', trim($page), $tt);
        $kernel = $tt[3][1];
        $db = new PDO('sqlite:'.dirname(__FILE__).'/tmpka.db');
        $sql = $db->query('SELECT lastkernel FROM tmpka')->fetch(PDO::FETCH_ASSOC);
        $sqlkernel = $sql['lastkernel'];
        if ($sqlkernel != $kernel) {
            $db->query("UPDATE tmpka SET lastkernel = '$kernel' WHERE id = 1");
            return LANG4 . $kernel . "\n~~~";
        } else {
            return LANG5 . $kernel . "\n~~~";
        }
    }

    // get roflcopter quotes
    function roflcopter() {
        $content = file_get_contents("http://roflcopter.pl/latest.json?per_page=1&page=1");
        $json = json_decode($content, true);
        return $json['rofls'][0]['content'] . "\n~~~";
    }

    // get weather
    function weather($code) {
        $xml = simplexml_load_string(file_get_contents('http://weather.yahooapis.com/forecastrss?w='.$code.'&u=c'));
        $xml->registerXPathNamespace('yweather', 'http://xml.weather.yahoo.com/ns/rss/1.0');
        $location = $xml->channel->xpath('yweather:location');
        foreach ($xml->channel->item as $item) {
            $current = $item->xpath('yweather:condition');
            $forecast = $item->xpath('yweather:forecast');
            $current = $current[0];
            $content = <<<EOP
Pogoda dla {$location[0]['city']}, {$location[0]['region']}, {$current['date']}
     - Obecna temperatura: {$current['temp']}*C
     - Prognoza
        {$forecast[0]['day']} - {$forecast[0]['text']}. High: {$forecast[0]['high']}*C Low: {$forecast[0]['low']}*C
        {$forecast[1]['day']} - {$forecast[1]['text']}. High: {$forecast[1]['high']}*C Low: {$forecast[1]['low']}*C\n~~~
EOP;
            return $content;
        }
    }


    function tlumacz($wyraz, $z, $na) {
	// please email me, for this function, sebastian@korotkiewicz.eu
	return $wyraz;
    }


}
?>
